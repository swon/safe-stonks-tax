from bs4 import BeautifulSoup
from dotenv import load_dotenv
import os
import requests


load_dotenv('.env', verbose=True)
session = requests.Session()

# Set Fidelity credentials and then login
payload = {
    'DEVICE_PRINT': '',
    'SavedIdInd': 'N',
    'SSN': os.getenv('FIDELITY_USERNAME'), 
    'PIN': os.getenv('FIDELITY_PASSWORD')
}
fidelity_login_url = 'https://login.fidelity.com/ftgw/Fas/Fidelity/RtlCust/Login/Response/dj.chf.ra'
s = session.post(url=fidelity_login_url, data=payload, headers=dict(referer='https://login.fidelity.com'))
s = session.get('https://oltx.fidelity.com/ftgw/fbc/oftop/portfolio#positions')

# Parse the response for bs4 and get all ticker symbols
soup = BeautifulSoup(s.text, 'html.parser')
raw_tickers = soup.find_all('h4')
print(s.text)
