# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0] - 2021-03-26
### Added
- Three basic commands for your entertainment (and to see how yolo my stonk picks are): $taxme, $swon, and $cursed
- Gitlab CI/CD - So much better than Jenkins CI
- First time DigitalOcean infrastructure, plznoflame

[Unreleased]: https://gitlab.com/swon/safe-stonks-tax/-/compare/1.0...master
[1.0]: https://gitlab.com/swon/safe-stonks-tax/-/releases/1.0
