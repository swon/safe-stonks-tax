variable "do_ssh_key" {
  description = "DigitalOcean SSH Key used to login"
  type        = string
  sensitive   = true
}

variable "gitlab_ci_token" {
  description = "Token used for Gitlab CI registration"
  type        = string
  sensitive   = true
}

variable "home_cidr" {
  description = "Home IP Address in CIDR form, usually ending in /32"
  type        = string
  sensitive   = true
}
