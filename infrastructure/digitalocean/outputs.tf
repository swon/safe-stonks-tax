output "bot_id" {
  value = digitalocean_droplet.bot.id
}

output "bot_ipv4_address" {
  value = digitalocean_droplet.bot.ipv4_address
}

output "bot_private_ipv4_address" {
  value = digitalocean_droplet.bot.ipv4_address_private
}

output "bot_urn" {
  value = digitalocean_droplet.bot.urn
}
