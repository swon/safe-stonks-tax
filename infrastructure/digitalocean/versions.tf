terraform {
  required_version = "0.14.9"

  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "smwon"

    workspaces {
      name = "safe-stonks-tax"
    }
  }

  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.6.0"
    }
  }
}

variable "do_token" {
  description = "DigitalOcean token"
  type        = string
  sensitive   = true
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}
