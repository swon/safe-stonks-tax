data "template_file" "gitlab_runner_userdata" {
  template = file("${path.module}/scripts/install-gitlab-runner.yml")

  vars = {
    gitlab_ci_token = var.gitlab_ci_token
  }
}

resource "digitalocean_project" "this" {
  name        = "SafeStonksTax"
  description = "Infrastructure for simple Discord bot to recommend stonks"
  purpose     = "Service or API"
  environment = "Production"
}

resource "digitalocean_droplet" "bot" {
  image              = "ubuntu-20-04-x64"
  name               = "safe-stonks-tax-bot"
  region             = "nyc1"
  size               = "s-1vcpu-1gb"
  private_networking = false
  ssh_keys           = [var.do_ssh_key]
  tags               = ["terraform", "python", "service"]
  user_data          = data.template_file.gitlab_runner_userdata.rendered
}

resource "digitalocean_firewall" "bot" {
  name = "only-22-80-and-443"

  droplet_ids = [digitalocean_droplet.bot.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = [var.home_cidr]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_project_resources" "this" {
  project = digitalocean_project.this.id
  resources = [
    digitalocean_droplet.bot.urn
  ]
}
