from dotenv import load_dotenv
import discord
import os
import random


load_dotenv('.env', verbose=True)
client = discord.Client()

def get_swon_holdings():
    # TODO Replace with scraping in Fidelity using requests
    holdings = [
        '100 BAC230120P18',
        '10 CLF220121C35',
        '50 COST',
        '15 CS230120P10',
        '100 DB230120P7',
        '2 DLTR',
        '214 GME (+132 in Roth IRA, +250 in Trad IRA)',
        '1 GME210702C220',
        '1 GME210917C680',
        '10 GME220121C950',
        '10 JPM230120P80',
        '3 KHC',
        '10 KHC220121C50',
        '109 PFE',
        '95.772 PLTR',
        '5 PLTR220121C40',
        '25 TBT230120C30',
        '30 UBS220121P15'
    ]
    holdings_str = '\n  * '.join(holdings)
    return(holdings_str)

def pick_random_safe_stock():
    safe_stocks = ['COST', 'TSLA', 'BA', 'SQ', 'NKE', 'AMD', 'SBUX', 'AAPL', 'MSFT', 'PFE', 'HD', 'DLTR', 'AMZN']
    return(random.choice(safe_stocks))

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    # TODO Replace GME with something that detects common tickers in WSB
    tax_cmd_invoked = message.content.startswith('$taxme')
    swon_bought_risky = message.author.name == 'swon' and 'GME' in message.content
    if tax_cmd_invoked or swon_bought_risky:
        safe_stock_ticker = pick_random_safe_stock()
        await message.channel.send(f'Tax: 10% to {safe_stock_ticker}')

    if message.content.startswith('$swon') or message.content.startswith('$cursed'):
        swon_holdings = get_swon_holdings()
        await message.channel.send(f'SWon currently holds the following:\n  * {swon_holdings}')

client.run(os.getenv('TOKEN'))
