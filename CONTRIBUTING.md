# Contributing to safe-stonks-tax

Would you like to help improve the safe-stonks-tax bot? Great! Here's an overview on how best to do so.

A lot of verbage in this CONTRIBUTING.md has been copied from https://github.com/reddit/node-private/blob/master/CONTRIBUTING.md. All credit on any elegance goes to that author and any garbage goes to me.

## What to change

Here's some examples of things you might want to make a pull request for:

    New features
    Bugfixes
    Inefficient blocks of code

If you have a more deeply-rooted problem with how the program is built or some of the stylistic decisions made in the code, it's best to create an issue before putting the effort into a pull request. The same goes for new features - it is best to check the project's direction, existing pull requests, and currently open and closed issues first.

## Style

    Four spaces for Python files. All standard code conventions should be followed.
    Two spaces, not tabs for all other files
    Bot should be functional after each commit to master

Look at existing code to get a good feel for the patterns used. 

## Using Git appropriately

    Clone the repository
    Create a topical branch using Git flow, but describe in the branch name what you're doing (e.g. hotfix/update-safe-stocks)
    Make your changes, committing at logical breaks.
    Push your branch
    Create a pull request
    Watch for comments or acceptance

Please make separate branches for unrelated changes!

## Licensing

safe-stonks-tax is Unlicensed. See details in the LICENSE file. This is a very permissive scheme.
