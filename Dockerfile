FROM python:3.9
EXPOSE 443

ARG TOKEN
ENV TOKEN "$TOKEN"

WORKDIR /app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY bot.py .

CMD [ "python", "./bot.py"]
